import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './style';
import { Component } from 'preact';
import { MainLayout } from './components/mainLayout';
import { VoucherAction } from './views/VoucherActions';
import { Settings } from './views/Settings';
import { FlashMessage } from './components/flashMessage/index';

export default class App extends Component {
  constructor(props) {
    super();
    this.state = {
      apiKey: '',
      domainId: null,
      locale: 'en',
      activeRouteComponent: 'Settings'
    };
  }
  isApiKey = () => this.state.apiKey.length > 0;
  onLocaleChange = locale => this.setState({ locale });
  changeRoute = activeRouteComponent => this.setState({ activeRouteComponent });
  updateApiKey = apiKey => this.setState({ apiKey });
  updateDomainId = domainId => this.setState({ domainId });
  render() {
    const RenderError = props =>
      !this.isApiKey() && <FlashMessage type={'danger'} msg={'Set API_Key'} />;
    return (
      <div>
        <MainLayout
          active={this.state.activeRouteComponent}
          onSelectNav={this.changeRoute}
          onLocaleChange={this.onLocaleChange}
        >
          <RenderError />
          <div className="container">{this.Router()}</div>
        </MainLayout>
      </div>
    );
  }

  Router() {
    switch (this.state.activeRouteComponent) {
    case 'Settings':
      return (
        <Settings
          updateDomainId={this.updateDomainId}
          updateApiKey={this.updateApiKey}
          apiKey={this.state.apiKey}

        />
      );
    default:
      return <VoucherAction apiKey={this.state.apiKey} />;
    }
  }
  loadState = () => {
    if (window.sessionStorage) {
      const state = window.sessionStorage.getItem('state');
      this.setState(state);
    }
  }
  saveState = (state) => {
    if (state) this.saveState({ ...state });
    if (window.sessionStorage) window.sessionStorage.setItem('state', this.state);
  }


  componentWillUnmount(){
    this.saveState(this.state);
  }
  componentWillMount(){
    this.loadState();
  }
}
