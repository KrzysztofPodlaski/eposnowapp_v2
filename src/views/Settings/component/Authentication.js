import { Component } from 'preact';

import { VoucherCartService } from '../../../service/voucherCart.service';

export class Authentication extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loggedAs: null,
      haveDomain: false,
      domains: []
    };
    this.vcService =  new VoucherCartService();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="login">Email</label>
            <input
              id="login"
              type="text"
              className="form-control"
              required
              ref={login => (this.login = login)}

            />
          </div>
          <div className="form-group">
            <button
              type="button"
              className="btn btn-primary"
              onClick={this.loadDomain}
            >
              Next
            </button>
          </div>
          <div className="form-group" hidden={!this.state.domains.length>0}>
            <label htmlFor="password">Password</label>
            <input
              id="password"
              type="password"
              className="form-control"
              required
              ref={password => (this.password = password)}
            />
          </div>
          <div className="form-group" hidden={this.state.domains<1}>
            <label htmlFor="domain">Domain</label>
            <select

              id="domain"
              className="form-control"
              name="domain"
              ref={select => (this.select = select)}
            >
              {this.state.domains.map(domain => <option value={domain.name}>{domain.name}</option>)}
            </select>

          </div>
          <div className="form-group" hidden={!this.state.domains.length>0}>
            <button
              className="btn btn-primary"
              disabled={this.state.domains.length===0}
            >Authenticate</button>
          </div>
        </form>
      </div>
    );
  }

  onSubmit = e => {
    e.preventDefault();

    this.vcService
      .getJWT(this.login.value, this.password.value, this.select.value)
      .then(({ accessToken }) => {
        this.props.updateApiKey(accessToken);
        this.props.updateDomainId(this.state.domains.find(d => d.name === this.select.value).id);
        this.props.flashMsg('success', 'Authentication success');
        this.setState({
          loggedAs: this.login.value,
          haveDomain: false,
          domains: []
        });
      })
      .catch(e => this.props.flashMsg('danger', e.message));
    [this.login, this.password].forEach(it => (it.value = ''));
  };
  loadDomain = (e) => {
    this.setState({ domains: [] });
    this.vcService
      .getUserDomain(this.login.value)
      .then(domains => this.setState({ domains  }))
      .catch(err => console.log('fetch domain error'));
  }
}
