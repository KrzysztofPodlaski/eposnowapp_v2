import $ from 'jquery';

import { Component } from 'preact';
import { Authentication } from './component/Authentication';
import { FlashMessage } from '../../components/flashMessage/index';

export class Settings extends Component {
  componentWillMount() {
    if (this.props.apiKey) this.setState({ API_KEY: this.props.apiKey });
  }

  constructor() {
    super();
    console.log(this.props);
    this.state = {
      isFlashMessageVisible: false,
      flashMessageType: 'info',
      flashMessageMsg: 'some danger info',
      API_KEY: ''
    };
  }

  isApiKey = () => this.props.apiKey.trim().length > 0;

  render() {
    return (
      <div className="settings-wrapper">
        <h2>Settings</h2>
        {this.state.isFlashMessageVisible && (
          <FlashMessage
            type={this.state.flashMessageType}
            msg={this.state.flashMessageMsg}
          />
        )}
        <hr />
        <div className="panel panel-default">
          <div className="panel-heading">
            <div className="form-group">
              <label
                htmlFor="api-key"
                className={this.isApiKey() ? '' : 'label-danger'}
              >
                {this.isApiKey()
                  ? 'Your API KEY'
                  : 'Missing API KEY provide one or login to get new'}
              </label>
              <input
                id="api-key"
                type="text"
                placeholder="Your API Key"
                className="form-control"
                defaultValue={
                  this.isApiKey() ? 'XXXXXXXX-XXXXXXXX-XXXXXXXXX-XXXXXXX' : ''
                }
                ref={input => (this.apiKeyInput = input)}
              />
            </div>
            <div className="form-group">
              <button
                type="button"
                className="btn btn-success"
                onClick={e => {
                  this.props.updateApiKey(this.apiKeyInput.value);
                  this.apiKeyInput.value = 'XXXXXXXXXXXXXXXX-XXXXXXXXXXX';
                  this.displayError('success', 'Key saved');
                }}
              >
                Save Key
              </button>
            </div>
          </div>
        </div>
        <div className="panel panel-default">
          <div
            className="panel-heading"
            onClick={e => $('#api-body').toggle(300)}
          >
            <button type="button" className="btn btn-primary">
              Get new API key
            </button>
          </div>
          <div id="api-body" hidden className="panel-body">
            <Authentication
              flashMsg={this.displayError}
              updateApiKey={this.props.updateApiKey}
              updateDomainId={this.props.updateDomainId}
            />
          </div>
        </div>
      </div>
    );
  }

  displayError = (flashMessageType, flashMessageMsg) => {
    setTimeout(() => this.setState({ isFlashMessageVisible: false }), 2000);
    this.setState({
      flashMessageType,
      flashMessageMsg,
      isFlashMessageVisible: true
    });
  };
}
