import $ from 'jquery';

export const TransactionInfo = props => (
  <div>
    <div className="panel panel-info">
      <div
        className="panel-heading"
        onClick={e => $('#transactionInfoBody').toggle(300)}
        style={{ cursor: 'pointer' }}
      >
        Transaction Info
      </div>
      <div id="transactionInfoBody" className="panel-body">
        <ul className="list-group">
          <li className="list-group-item list-group-item-info">
            Item 1 - <span>Value</span>{' '}
          </li>
          {props.tenders &&
            props.tenders.map(it => (
              <li className="list-group-item list-group-item-danger">
                {it.value}
              </li>
            ))}
          <li className="list-group-item list-group-item-info">Item 3 - </li>
        </ul>
      </div>
    </div>
  </div>
);
