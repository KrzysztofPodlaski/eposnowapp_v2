import { Component } from 'preact';
import { VoucherCartService } from '../../../service/voucherCart.service';

const debug = true;

export class VoucherChecker extends Component {
  constructor() {
    super();
    this.voucherCartService = new VoucherCartService();
    this.state = {
      voucherCode: ''
    };
  }
  onKeyDown = e => this.setState({ voucherCode: e.target.value });
  onSubmit = e => {
    e.preventDefault();
    debug && console.log(`Value to check: ${this.state.voucherCode}`);
    this.voucherCartService
      .fetchVoucherInfo(this.state.voucherCode)
      .then(this.props.onVoucherFetch)
      .catch(e => console.log(e)); // eslint:disable-line
  };
  render() {
    return (
      <div className="panel panel-default ">
        <form className="form panel-body  ">
          <div className="form-group">
            <label htmlFor="voucherCode">Voucher Code</label>
            <div className="row">
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  onKeyUp={this.onKeyDown}
                  placeholder="Enter Voucher code:"
                />
              </div>
              <div className="col-sm-2">
                <button className="btn btn-success" onClick={this.onSubmit}>
                  Check
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
