import { Component } from 'preact';

export class VoucherList extends Component {
  reedemVoucher = v => {};
  render() {
    const VoucherRow = props =>
      this.props.vouchers.map(v => (
        <tr key={v.code}>
          <td>{v.code}</td>
          <td>{v.title}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={e => this.reedemVoucher(v)}
            >
              Reedem
            </button>
          </td>
        </tr>
      ));

    if (this.props.vouchers.length === 0) return;
    return (
      <div className="voucher-list">
        <div className="panel panel-default">
          <table className="table table-hover table-responsive table-striped">
            <thead>
              <tr>
                <th>Voucher</th>
                <th>Voucher Title</th>
                <th>Voucher Action</th>
              </tr>
            </thead>
            <tbody>
              <VoucherRow />
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
