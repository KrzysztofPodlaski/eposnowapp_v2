import { Component } from 'preact';
import { TransactionInfo, VoucherChecker, VoucherList } from './components';

export class VoucherAction extends Component {
  someFunction = () => {
    console.log('hello');
  };

  constructor() {
    super();
    this.state = {
      operationComplete: false,
      voucherList: [],
      tenders: []
    };
  }

  componentWillMount() {}
  componentWillUnmount() {
    console.log('will unmount');
    if (this.state.operationComplete) {
      window.localStorage.setItem('state', this.state);
      window.localStorage.setItem('saveTime', Date.now());
    }
    else window.localStorage.removeItem('state');
  }

  render() {
    return (
      <div>
        <div className="container">
          <h2>VoucherAction</h2>
          {this.props.apiKey.trim().length > 0 && (
            <VoucherChecker onVoucherFetch={this.onVoucherFetch} />
          )}
          <TransactionInfo tenders={this.state.tenders} />
          <VoucherList
            vouchers={this.state.voucherList}
            onReedemVoucher={this.onReedemVoucher}
          />
        </div>
      </div>
    );
  }
  onReedemVoucher = tender => {
    this.setState({ tenders: [...this.state.tenders, tender] });
  };
  onVoucherFetch = voucher => {
    this.setState({ voucherList: [...this.state.voucherList, voucher] });
  };
}
