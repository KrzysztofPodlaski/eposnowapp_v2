import fetch from 'isomorphic-fetch';
import b64 from 'base-64';

import {paramToUrl} from '../utils/urlParamToString';

import {API_VC_BASE, API_VC_KEY, API_VC_VERSION, ERROR} from '../utils/const';

export class VoucherCartService {
  constructor(defaultHeader) {
    this.defaultHeader = new Headers({Authorization: `Bearer ${API_VC_KEY}`});

  }

  fetchVoucherInfo(voucherCode) {
    return fetch(
      [API_VC_BASE, API_VC_VERSION, 'voucher', voucherCode].join('/')
    ).then(response => response.json());
  }

  getJWT(login, password, domain) {
    const token = b64.encode(''.concat(login, ':', password));
    const headers = new Headers({Authorization: `Basic ${token}`});
    const body = new FormData();
    body.append('subdomain', domain);
    const mode = 'cors';
    const url = [API_VC_BASE, API_VC_VERSION, 'authorize'].join('/');
    const method = 'POST';
    return fetch(url, {body, headers, method, mode})
      .then(result => result.json())
      .then(json => {
        console.log('In result->json', json);
        if (json.status && json.status === 'authenticated')
          return json;
        throw 'Cant Authorize';
      })
      .catch(e => {
        throw new Error(ERROR.AUTHENTICATION_MSG, ERROR.AUTHENTICATION);
      });
  }

  getUserDomain(email) {
    return fetch([API_VC_BASE, API_VC_VERSION, 'user-domains'].join('/') + paramToUrl({email}))
      .then(resp => resp.json())
      .then(json => {
        console.log(json);
        if (json.status === 'success') return json.domains;
        return new Error('Domain not found', ERROR.DOMAIN);
      });
  }
}
