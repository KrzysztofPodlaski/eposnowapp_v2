export const paramToUrl = paramsAsObject => {
  let result =[];
  Object.entries(paramsAsObject).forEach(([k, v]) => {
    result.push(`${k}=${v}`);
  });
  return `?${result.join('&')}`;
}
