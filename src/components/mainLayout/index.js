import { Component } from 'preact';
import { Footer, Header } from './components';

export class MainLayout extends Component {
  constructor() {
    super();
    this.state = {
      appName: 'Eposnow Voucher Reedem'
    };
  }

  render() {
    return (
      <div className="main-wrapper">
        <Header
          active={this.props.active}
          onSelectNav={this.props.onSelectNav}
        />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}
