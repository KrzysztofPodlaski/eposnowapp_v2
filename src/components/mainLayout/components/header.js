export const Header = props => (
  <header>
    <nav className="navbar navbar-default">
      <div className="container">
        <div class="navbar-header">
          <button
            type="button"
            class="navbar-toggle collapsed"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-expanded="false"
          >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" />
            <span class="icon-bar" />
            <span class="icon-bar" />
          </button>
          <a class="navbar-brand" href="#">
            Brand
          </a>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="nav navbar-nav">
            <li
              class={props.active === 'VoucherAction' ? 'active ' : ' '}
              onClick={() => props.onSelectNav('VoucherAction')}
            >
              <a href="#">Home</a>
            </li>
            <li
              class={props.active === 'Settings' ? 'active ' : ' '}
              onClick={() => props.onSelectNav('Settings')}
            >
              <a href="#">Settings</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
);
