import './style.css';

export const FlashMessage = ({ type, msg, closeCb }) => (
  <div className="flash-wrapper">
    <div className={`panel panel-${type}`}>
      <div className="panel-heading">
        <p>{msg}</p>
      </div>
    </div>
  </div>
);
