import { Component } from 'preact';

import './style.less';
export class Modal extends Component {
  constructor() {
    super();
    this.state = {
      isVisable: this.props.isVisable
    };
  }

  render() {
    if (!this.props.isVisable) return;
    return (
      <div className="modal-overflow">
        <div className="modal-window">
          <div className="panel panel-default">
            <div className="modal-heading panel-heading">
              {this.props.modalTitle}
            </div>
            <div className="modal-body panel-body">{this.props.children}</div>
            <div className="panel-footer">
              <button id="ok" className="btn btn-primary">
                OK
              </button>
              <button
                id="cancel"
                className="btn btn-primary"
                ref={button => (this.cancelButton = button)}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
